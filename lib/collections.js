SimpleSchema.debug = true; 


var Schemas = {};

UI.registerHelper("Schemas", Schemas);

// for reference see documents collection in dailiesLabAuto
Books = new Meteor.Collection("books", {
	schema: {
		rootPath: {
			type: String,
			label: "Root Path",
			max:250
		},
		headerFile: {
			type: String,
			label: "Header File",
			max: 200,
			optional: true
		},
		title: {
			type: String,
			label: "Title",
			max: 200
		},
		requiredEmail: {
			type: String,
			regEx: SimpleSchema.RegEx.Email,
			label: "Author E-mail address"
		},
		
		author: {
			type: String,
			label: "Author Name"
		},
		slide_01: {
		  type: String,
		  label: "Slideshow Image 01"
		},
		
		slide_02: {
		  type: String,
		  label: "Slideshow Image 02"
		},
		
		slide_03: {
		  type: String,
		  label: "Slideshow Image 03"
		},
        // image : {
        //       type : String,
        //       label : "Test Image",
        //       regEx : SchemaRegEx.FilePickerImageUrl,
        //       optional: true // the RegEx defined here defines the image field
        // },
        // url: {
        //     type: String,
        //     label: "Image URL",
        //     optional : true
        // },

		slide_main: {
		  type: String,
		  label: "Main Slideshow Image"
		},
		
		tags: {
		  type: [String],
		  label: "Top Story Tags ",
		  minCount: 1
		},

		related_infographic: {
		  type: String,
		  label: "Related Infographic"
		},

		related_video: {
		  type: String,
		  label: "Related video"
		},

		related_comic: {
		  type: [String],
		  label: "Related Comic Image"
		},

		related_joke_text: {
		  type: String,
		  label: "Related Joke Text", 
		  max: 1000
		},

		related_joke_image: {
		  type: String,
		  label: "Related Joke Image"
		},

		story_text: {
		  type: String,
		  label: "Story Text",
		  max: 2000
		},

		summary: {
			type: String,
			label: "Brief summary",
			max: 1000
		},
	    radioBoolean: {
		    type: Boolean,
		    label: "Random Test Radio",
		    optional: true,
		    defaultValue: true
    	},

		createdAt: {
			type: Date,
			autoValue: function () {
			  if (this.isInsert) {
				return new Date;
			  } else {
				this.unset();
			  };
			},     

		}
	}
});

Books.allow({
  insert: function() {
    return true;
  },
  update: function() {
    return true;
  },
  remove: function() {
    return true;
  },
  fetch: []
});

Books.simpleSchema().messages({
  notUnique: "Only one of each allowed"
});


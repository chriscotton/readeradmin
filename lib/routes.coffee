Router.map ->
  @route "home",
    path: "/"
    template: "home"
    layoutTemplate: "mainLayout"
  
  @route "add_top_story",
    path: "/add_top_story"
    template: "home"
    layoutTemplate: "mainLayout"

  @route "update_top_story",
    path: "/update_top_story"
    template: "updateBookForm"
    layoutTemplate: "mainLayout"
    waitOn: ->
      Meteor.subscribe "books"

    data: ->

  @route "remove_top_stories",
    path: "/remove_top_stories"
    layoutTemplate: "mainLayout"
    template: "removeBookForm"
    onBeforeAction: ->
      Session.set "currentAction", "delete"
    waitOn: ->
      Meteor.subscribe "books"

    data: ->
      templateData = 
        books: Books.find {},
          sort:
            createdAt:-1
            title:1
      
      # Session.set key, value
      # console.log "SLIDE MAIN IN ROUTE DATA" + templateData.this._id.slide_main
      templateData



  # basename that bitch for now.....
# "C:\fakepath\D10-2714map.jpg"
# basename = (path) -> 
#   path.replace(/\\/g, '/').replace(/.*\//, '')

# Template.hello.greeting = function () {
#   return "sdfljj";
# };

# arr2 = {}
# arr2["slide_01"] = doc.slide_01.value # equivalent to arr2.foo="Bar"
# arr2["slide_02"] = doc.slide_02.value # equivalent to arr2.bar="Foo"
# arr2["slide_03"] = doc.slide_01.value # equivalent to arr2.bar="Foo"
# arr2["slide_main"] = doc.slide_main.value # equivalent to arr2.bar="Foo"
# arr2["infographic"] = doc.related_infographic.value # equivalent to arr2.bar="Foo"
# arr2["joke_image"] = doc.related_joke_image.value # equivalent to arr2.bar="Foo"
# console.log ("GOT HERE !!! ")
# str = "values are : "
# for myVal in arr2
#   str += myVal + " |"
#   console.log str + ': ' + arr2.myVal
# # basename()





Router.map(function() {
	this.route('home', 
		{path: '/',
		 template: 'home',
		 layoutTemplate: 'mainLayout'
		}),

	this.route('add_top_story', 
		{path: '/add_top_story',
		 template: 'home',
		 layoutTemplate: 'mainLayout'
		}),

		this.route('update_top_story', 
		{path: '/update_top_story',
		 template: 'updateBookForm',
		 layoutTemplate: 'mainLayout',
		 waitOn: function (){
			return Meteor.subscribe('books')
		 }, 

		 data: function(){

		 }
		})

		this.route('remove_top_stories', 
		{
			 path: '/remove_top_stories',
			 waitOn: function() {
					return Meteor.subscribe('books')
			 },

			 data: function() {
					templateData = {books: Books.find({}, {sort: {submitted: -1}})}
					Session.set("currentAction", "delete")
					return templateData
			 },

			 onBeforeAction: function(){

			 },
			 template: 'removeBookForm',
			 layoutTemplate: 'mainLayout'
		})

});
if Meteor.isClient
	Meteor.subscribe "books"
	Session.setDefault "currAction", "insert"
	Session.set "CCDEBUG", true
	Session.set "selectedDocId", null
	
	unmanglePath = (path, rootDir)->
		newPath = path.replace /C\:\\fakepath\\/g, "\/"+rootDir+"\/"

	Template.removeBookForm.helpers
		clean_slide_main:->
			unmanglePath(this.slide_main, this.rootPath)
			# ...
	Template.removeBookForm.deleteDocMode = ->
		if Session.get("currAction") is "delete"
			true
		else
			false

	Template.removeBookForm.events
		"click .updateStoryButton": (e, t) ->
			e.preventDefault()
			
			# AutoForm.resetForm("docForm");
			console.log "in the updateStoryButton click event!!!!"
			Session.set "selectedDocId", @_id
			console.log " selectedDocId  is " + Session.get("selectedDocId")
			Router.go "update_top_story"
			return

		"click .deleteStoryButton": (e, t) ->
			e.preventDefault()
			
			# AutoForm.resetForm("docForm");
			console.log "in the deleteStoryButton click event!!!!"
			Session.set "selectedDocId", null
			return

	Template.removeBookForm.deleteButtonType = ->
		if Session.get("currAction") is "delete"
			"danger"
		else
			"primary"

	Template.insertBookForm.rendered = -> # built in metepr function much like document ready for templates
		$(".full-text").wysihtml5
			"font-styles": true #Font styling, e.g. h1, h2, etc. Default true
			emphasis: true #Italics, bold, etc. Default true
			lists: true #(Un)ordered lists, e.g. Bullets, Numbers. Default true
			html: true #Button which allows you to edit the generated HTML. Default false
			link: true #Button to insert a link. Default true
			image: true #Button to insert an image. Default true,
			color: true #Button to change color of font
			size: "sm" #Button size like sm, xs etc.

		return

	Template.updateBookForm.rendered = -> # built in metepr function much like document ready for templates
		$(".full-text").wysihtml5
			"font-styles": true #Font styling, e.g. h1, h2, etc. Default true
			emphasis: true #Italics, bold, etc. Default true
			lists: true #(Un)ordered lists, e.g. Bullets, Numbers. Default true
			html: true #Button which allows you to edit the generated HTML. Default false
			link: true #Button to insert a link. Default true
			image: true #Button to insert an image. Default true,
			color: true #Button to change color of font
			size: "sm" #Button size like sm, xs etc.

		return

	Template.updateBookForm.editingDoc = ->
		Books.findOne _id: Session.get("selectedDocId")


	AutoForm.addHooks [
		"insertBookForm"
		"updateBookForm"
		"removeBookForm"
	],
		before:
			insert: (doc) ->
				console.log "before.insert RECIEVED DOC!! ", doc
				doc

			docToForm: (doc) ->
				doc.tags = doc.tags.join(", ")  if _.isArray(doc.tags)
				doc

			formToDoc: (doc) ->
				doc.tags = doc.tags.split(",")  if typeof doc.tags is "string"
				doc

			remove: (id) ->
				name = Books.findOne(id).title
				console.log " Story Title to confirm is " + name
				Session.set "currAction", "delete"
				console.log "currAction is " + currAction
				confirm "Remove Story" + name + "?"

# end is isClient

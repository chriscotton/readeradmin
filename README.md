## Kids Dailies Reader Admin App

This is here to give you an overview of The Reader Admin App project,  Clone this repo to get started. 

## How to install and run

Install Meteorite (if not already installed)
```
npm install -g meteorite
```
Start front-end from project root 
```
mrt -p <optional port number>
```
Default port is 3000. To view it 
```
htp://localhost:3000
```
### Packages used

* semantic-ui
* collection2
* less
* jquery
* underscore
* handlebar-helpers
* iron-router
* iron-router-progress
* accounts-password
* accounts-ui
* autoform

## What's in this project

The "insecure" and "autopublish" packages are removed by default. Several other packages are added, which are listed on the bottom. 

### Features

* Comprehensive folder structure
* TDD / BDD with [laika](http://arunoda.github.io/laika/)
* Multi page apps with [iron-router](https://github.com/EventedMind/iron-router)
* A way to load fixtures (as of now no external packages used for that)
* meteor-boilerplate console tool, which helps on creating views, routes and so on (meteor-boilerplate.bat for windows users)
* realtime updates of topstories in english. ( Must have content demo pack if running offsite ) 

When run on port 3111 this project publishes collection data via ddp to reader front-end for display.